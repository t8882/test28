<?php

namespace Tests\Feature;

use App\Models\Car;
use Illuminate\Http\Response;
use Tests\TestCase;

class CarTest extends TestCase
{
    const URI = '/api/v1/cars';
    const CARS_TABLE_NAME = 'cars';

    public function test_that_get_all_cars_returns_data_in_valid_format()
    {
        $this
            ->json('get', static::URI)
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'success',
                    'cars' => [
                        'count',
                        'data' => [
                            '*' => [
                                'id',
                                'manufacturer_id',
                                'model_id',
                                'year',
                                'mileage',
                                'color'
                            ]
                        ],
                        'pagination' => [
                            'total',
                            'count',
                            'per_page',
                            'current_page',
                            'total_pages'
                        ]
                    ]
                ]
            );
    }

    public function test_that_car_is_created_successfully()
    {
        $payload = [
            'manufacturer' => 'Toyota',
            'model' => 'Camry',
            'year' => 1995,
            'mileage' => 201111,
            'color' => 'black'
        ];
        $recordDB = [
            'manufacturer_id' => 1,
            'model_id' => 1,
            'year' => 1995,
            'mileage' => 201111,
            'color' => 'black'
        ];
        $this
            ->json('post', static::URI, $payload)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'success',
                    'car' => [
                        'id',
                        'manufacturer_id',
                        'model_id',
                        'year',
                        'mileage',
                        'color'
                    ]
                ]
            );
        $this->assertDatabaseHas(static::CARS_TABLE_NAME, $recordDB);
    }

    public function test_that_car_is_retrieved_successfully()
    {
        $car = Car::create([
            'manufacturer_id' => 1,
            'model_id' => 2,
            'year' => 2000,
            'mileage' => 75000,
            'color' => 'yellow'
        ]);

        $this
            ->json('get', static::URI . '/' . $car->id)
            ->assertStatus(Response::HTTP_OK)
            ->assertExactJson(
                [
                    'success' => true,
                    'car' => [
                        'id' => $car->id,
                        'manufacturer_id' => $car->manufacturer_id,
                        'model_id' => $car->model_id,
                        'year' => $car->year,
                        'mileage' => $car->mileage,
                        'color' => $car->color,
                    ]
                ]
            );
    }

    public function test_that_car_is_updated_successfully()
    {
        $payload = [
            'manufacturer_id' => 1,
            'model_id' => 2,
            'year' => 2000,
            'mileage' => 75000,
            'color' => 'yellow'
        ];
        $car = Car::create($payload);
        $payloadForUpdate = [
            'manufacturer' => 'Toyota',
            'model' => 'Camry',
            'year' => 2004,
            'color' => 'green'
        ];
        $carRecordInDB = [
            'manufacturer_id' => 1,
            'model_id' => 1,
            'year' => 2004,
            'mileage' => 75000,
            'color' => 'green'
        ];

        $this
            ->json('patch', static::URI . '/' . $car->id, $payloadForUpdate)
            ->assertStatus(Response::HTTP_OK)
            ->assertExactJson(
                [
                    'success' => true,
                    'car' => [
                        'id' => $car->id,
                        'manufacturer_id' => $carRecordInDB['manufacturer_id'],
                        'model_id' => $carRecordInDB['model_id'],
                        'year' => $carRecordInDB['year'],
                        'mileage' => $carRecordInDB['mileage'],
                        'color' => $carRecordInDB['color'],
                    ]
                ]
            );
    }

    public function test_that_car_is_destroyed_successfully()
    {
        $payload = [
            'manufacturer_id' => 1,
            'model_id' => 2,
            'year' => 2000,
            'mileage' => 75000,
            'color' => 'yellow'
        ];
        $car = Car::create($payload);
        $payload['id'] = $car->id;

        $this
            ->json('delete', static::URI . '/' . $car->id)
            ->assertNoContent();

        $this
            ->assertDatabaseMissing(static::CARS_TABLE_NAME, $payload);
    }

}
