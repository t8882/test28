# Тестовое задание 28

Цель нашего тестового задания - это определить уровень ваших знаний и навыков по разработке бэкенда веб-приложения. Тестовое задание сформулировано без каких-то
жёстких требований, но просим при реализации учитывать лучшую практику из вашего опыта по оформлению кода и репозитория, обеспечению безопасности,
производительности и командной работе. Пожалуйста, помните, что код и API должны быть не только рабочими, но и стабильными, понятными и удобными в использовании.

## Модели данных
1. Марка автомобиля
- Название
2. Модель автомобиля
- Название
- Марка
3. Автомобиль
- Марка
- Модель
- Год выпуска (опциональный атрибут)
- Пробег (опциональный атрибут)
- Цвет (опциональный атрибут)

## API
1. Список марок автомобилей;
2. Список моделей автомобилей;
3. Список + CRUD автомобилей.

************************************************************

## Тесты
Тесты для CRUD 'Car' находятся в /tests/Feature/CarTest.php

## Эндпоинты:
- GET (получение списка производителей авто): http://127.0.0.1:8000/api/v1/manufacturers/

Пример ответа:

```json
{
  "success": true,
  "manufacturers": {
      "count": 4,
      "data":
      [
          {
              "id": 1,
              "name": "Toyota"
          },
          {
              "id": 2,
              "name": "Honda"
          },
          {
              "id": 3,
              "name": "BMW"
          },
          {
              "id": 4,
              "name": "Kia"
          }
      ]
  }
}
```
- GET (получение списка моделей авто): http://127.0.0.1:8000/api/v1/models/

Пример ответа:

```json
{
  "success": true,
  "car_models": {
      "count": 8,
      "data":
      [
          {
              "id": 1,
              "name": "Camry",
              "manufacturer_id": 1
          },
          {
              "id": 2,
              "name": "Prado",
              "manufacturer_id": 1
          },
          {
              "id": 3,
              "name": "Civic",
              "manufacturer_id": 2
          },
          {
              "id": 4,
              "name": "Accord",
              "manufacturer_id": 2
          },
          {
              "id": 5,
              "name": "3 series",
              "manufacturer_id": 3
          },
          {
              "id": 6,
              "name": "5 series",
              "manufacturer_id": 3
          },
          {
              "id": 7,
              "name": "Cerato",
              "manufacturer_id": 4
          },
          {
              "id": 8,
              "name": "Sorento",
              "manufacturer_id": 4
          }
      ]
  }
}
```

- POST (создание 'Car'): http://127.0.0.1:8000/api/v1/cars

Пример тела запроса:

```json
  {
      "manufacturer": "Toyota",
      "model": "Camry",
      "year": 2001
  }
```

Пример ответа:

```json
{
    "success": true,
    "car": {
        "id": 77,
        "manufacturer_id": 1,
        "model_id": 1,
        "year": 2001,
        "mileage": null,
        "color": null
    }
}
```

- GET (получение 'Car' по ID): http://127.0.0.1:8000/api/v1/cars/{id}

Пример ответа:

```json
{
    "success": true,
    "car": {
        "id": 14,
        "manufacturer_id": 4,
        "model_id": 7,
        "year": 2007,
        "mileage": null,
        "color": null
    }
}
```

- GET (получение списка 'Car'): http://127.0.0.1:8000/api/v1/cars/

Пример ответа:

```json
{
    "success": true,
    "cars": {
        "count": 3,
        "data": 
        [
            {
                "id": 10,
                "manufacturer_id": 2,
                "model_id": 3,
                "year": 2000,
                "mileage": 22222,
                "color": "green"
            },
            {
                "id": 11,
                "manufacturer_id": 2,
                "model_id": 3,
                "year": 2011,
                "mileage": 3333,
                "color": "green"
            },
            {
                "id": 12,
                "manufacturer_id": 4,
                "model_id": 7,
                "year": 2011,
                "mileage": 3333,
                "color": "black"
            }
        ],
        "pagination": {
            "total": 54,
            "count": 3,
            "per_page": 3,
            "current_page": 1,
            "total_pages": 18
            }
        }
}
```

- PATCH (изменение 'Car' по ID): http://127.0.0.1:8000/api/v1/cars/{id}

Пример тела запроса:

```json
{ 
    "manufacturer": "Kia",
    "model": "Cerato"    
} 
```

Пример ответа:

```json
{
    "success": true,
    "car": {
        "id": 15,
        "manufacturer_id": 4,
        "model_id": 7,
        "year": null,
        "mileage": null,
        "color": null
    }
}
```

- DELETE (удаление 'Car' по ID): http://127.0.0.1:8000/api/v1/cars/{id}


