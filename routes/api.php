<?php

use App\Http\Controllers\CarController;
use App\Http\Controllers\CarModelController;
use App\Http\Controllers\ManufacturerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::get('/manufacturers/{id}', [ManufacturerController::class, 'show']);
    Route::get('/manufacturers', [ManufacturerController::class, 'index']);
    Route::get('/models/{id}', [CarModelController::class, 'show']);
    Route::get('/models', [CarModelController::class, 'index']);
    Route::post('/cars/', [CarController::class, 'store']);
    Route::get('/cars/', [CarController::class, 'index']);
    Route::get('/cars/{id}', [CarController::class, 'show']);
    Route::delete('/cars/{id}', [CarController::class, 'destroy']);
    Route::patch('/cars/{id}', [CarController::class, 'update']);
});
