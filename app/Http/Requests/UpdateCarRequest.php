<?php

namespace App\Http\Requests;

use App\Models\Manufacturer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateCarRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'manufacturer' => [
                'sometimes',
                Rule::exists('manufacturers', 'name')
                    ->where('name', request()->get('manufacturer'))
            ],
            'model' => [
                'sometimes',
                Rule::exists('models', 'name')
                    ->where('manufacturer_id',
                        Manufacturer::where('name', request()->get('manufacturer'))
                            ->first()
                            ->id
                    )
            ],
            'year' => 'nullable|integer|gt:0',
            'mileage' => 'nullable|integer|gt:0',
            'color' => 'nullable|string',
        ];
    }

    public function messages()
    {
        return [
            'manufacturer' => 'Please provide a valid manufacturer',
            'model' => 'Please provide a valid model',
            'year' => 'Please provide a valid year',
            'mileage' => 'Please provide a valid mileage',
            'color' => 'Please provide a valid color',
        ];
    }
}
