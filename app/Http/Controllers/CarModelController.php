<?php

namespace App\Http\Controllers;

use App\Http\Resources\CarModelCollection;
use App\Http\Resources\CarModelResource;
use App\Models\CarModel;
use Illuminate\Http\Response;

class CarModelController extends Controller
{
    public function index()
    {
        $carModels = CarModel::with('manufacturer')->get();

        return response()->json(
            [
                'success' => true,
                'car_models' => new CarModelCollection($carModels)
            ]
        );
    }

    public function show($id)
    {
        $carModel = CarModel::query()->find($id);

        if (!$carModel) {
            return response()->json(
                [
                    'success' => false,
                    'message' => 'Car model does not exist'
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        return response()->json(
            [
                'success' => true,
                'car_model' => new CarModelResource($carModel)
            ]
        );
    }
}
