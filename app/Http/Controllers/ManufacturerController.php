<?php

namespace App\Http\Controllers;

use App\Http\Resources\ManufacturerCollection;
use App\Http\Resources\ManufacturerResource;
use App\Models\Manufacturer;
use Illuminate\Http\Response;

class ManufacturerController extends Controller
{
    public function index()
    {
        $manufacturers = Manufacturer::query()->get();
        return response()->json(
            [
                'success' => true,
                'manufacturers' => new ManufacturerCollection($manufacturers)
            ]
        );
    }

    public function show($id)
    {
        $manufacturer = Manufacturer::query()->find($id);
        if (!$manufacturer) {
            return response()->json(
                [
                    'success' => false,
                    'message' => 'Manufacturer does not exist'
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        return response()->json(
            [
                'success' => true,
                'manufacturer' => new ManufacturerResource($manufacturer)
            ]
        );
    }
}
