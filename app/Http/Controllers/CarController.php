<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCarRequest;
use App\Http\Requests\UpdateCarRequest;
use App\Http\Resources\CarCollection;
use App\Http\Resources\CarResource;
use App\Models\Car;
use App\Services\Car\Service;
use Exception;
use Illuminate\Http\Response;

class CarController extends Controller
{
    protected Service $service;
    protected int $carsPerPage = 3;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $cars = Car::paginate($this->carsPerPage);

        return response()->json(
            [
                'success' => true,
                'cars' => new CarCollection($cars)
            ]
        );
    }

    public function store(StoreCarRequest $request)
    {
        $data = $request->validated();
        try {
            $car = $this->service->store($data);
        } catch (Exception $exception) {
            return response()->json(
                [
                    'success' => false,
                    'message' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }

        return response()->json(
            [
                'success' => true,
                'car' => new CarResource($car)
            ],
            Response::HTTP_CREATED
        );
    }

    public function show(string $id)
    {
        $car = Car::query()->find($id);
        if (!$car) {
            return response()->json(
                [
                    'success' => false,
                    'message' => 'Car does not exist'
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        return response()->json(
            [
                'success' => true,
                'car' => new CarResource($car)
            ]
        );
    }

    public function update(UpdateCarRequest $request, string $id)
    {
        $data = $request->validated();
        try {
            $car = $this->service->update($data, $id);
        } catch (Exception $exception) {
            return response()->json(
                [
                    'success' => false,
                    'message' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }

        return response()->json(
            [
                'success' => true,
                'car' => new CarResource($car)
            ]
        );
    }

    public function destroy(string $id)
    {
        $car = Car::find($id);
        if (!$car) {
            return response()->json(
                [
                    'success' => false,
                    'message' => 'Car does not exist anymore'
                ],
                Response::HTTP_NOT_FOUND
            );
        }
        $car->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
