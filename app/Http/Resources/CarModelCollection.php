<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CarModelCollection extends ResourceCollection
{
    public function toArray(Request $request): array
    {
        $count = $this->collection->count();

        return [
            'count' => $count,
            'data' => $this->collection
        ];
    }
}
