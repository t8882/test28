<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CarResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'manufacturer_id' => $this->resource->manufacturer_id,
            'model_id' => $this->resource->model_id,
            'year' => $this->resource->year,
            'mileage' => $this->resource->mileage,
            'color' => $this->resource->color,
        ];
    }
}
