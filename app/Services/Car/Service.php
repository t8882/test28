<?php

namespace App\Services\Car;

use App\Models\Car;
use App\Models\CarModel;
use App\Models\Manufacturer;
use Exception;
use Illuminate\Http\Response;

class Service
{
    public function store($data)
    {
        try {
            $manufacturerId = Manufacturer::where('name', $data['manufacturer'])->first()->id;
            if (!$manufacturerId) {
                abort(Response::HTTP_BAD_REQUEST, 'Manufacturer with ID not found');
            }
            $carModelId = CarModel::where('name', $data['model'])->first()->id;
            if (!$carModelId) {
                abort(Response::HTTP_BAD_REQUEST, 'Car model with the ID not found');
            }
            $car = Car::create([
                'manufacturer_id' => $manufacturerId,
                'model_id' => $carModelId,
                'year' => $data['year'] ?? null,
                'mileage' => $data['mileage'] ?? null,
                'color' => $data['color'] ?? null,
            ]);
        } catch (Exception $exception) {
            abort($exception->getCode(), $exception->getMessage());
        }
        return $car;
    }

    public function update($data, $id)
    {
        try {
            $car = Car::find($id);
            if ((isset($data['manufacturer'])) && !(isset($data['model']))) {
                $manufacturerId = Manufacturer::where('name', $data['manufacturer'])->first()->id;
            }
            if (isset($manufacturerId)) {
                $carModelIdOfCarFromDB = Car::find($id)->model_id;
                $listOfCarModelId = CarModel::where('manufacturer_id', $manufacturerId)->get('id');
                if (!($listOfCarModelId->contains($carModelIdOfCarFromDB))) {
                    abort(Response::HTTP_BAD_REQUEST, 'Could not save: manufacturer is not valid');
                }
                $car->manufacturer_id = $manufacturerId;
            }
            if (!(isset($data['manufacturer'])) && isset($data['model'])) {
                $carModelId = CarModel::where('name', $data['model'])->first()->id;
            }
            if (isset($carModelId)) {
                $manufacturerIdFromRequest = CarModel::where('name', $data['model'])->first()->manufacturer_id;
                $manufacturerIdOfCarFromDB = Car::find($id)->manufacturer_id;
                if (!($manufacturerIdFromRequest === $manufacturerIdOfCarFromDB)) {
                    abort(Response::HTTP_BAD_REQUEST, 'Could not save: car model is not valid');
                }
                $car->model_id = $carModelId;
            }
            if ((isset($data['manufacturer'])) && (isset($data['model']))) {
                $manufacturerId = Manufacturer::where('name', $data['manufacturer'])->first()->id;
                $manufacturerIdFromRequest = CarModel::where('name', $data['model'])->first()->manufacturer_id;
                if (!($manufacturerId === $manufacturerIdFromRequest)) {
                    abort(
                        Response::HTTP_BAD_REQUEST,
                        'Could not save: manufacturer does not match car model'
                    );
                }
                $car->manufacturer_id = $manufacturerId;
                $car->model_id = CarModel::where('name', $data['model'])->first()->id;
            }
            if (isset($data['year'])) {
                $car->year = $data['year'];
            }
            if (isset($data['mileage'])) {
                $car->mileage = $data['mileage'];
            }
            if (isset($data['color'])) {
                $car->color = $data['color'];
            }
            $car->save();
        } catch (Exception $exception) {
            abort($exception->getCode(), $exception->getMessage());
        }
        return $car;
    }
}
