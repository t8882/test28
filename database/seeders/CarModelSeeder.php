<?php

namespace Database\Seeders;

use App\Models\CarModel;
use Illuminate\Database\Seeder;

class CarModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        CarModel::create(
            [
                'name' => 'Camry',
                'manufacturer_id' => 1
            ]
        );
        CarModel::create(
            [
                'name' => 'Prado',
                'manufacturer_id' => 1
            ]
        );
        CarModel::create(
            [
                'name' => 'Civic',
                'manufacturer_id' => 2
            ]
        );
        CarModel::create(
            [
                'name' => 'Accord',
                'manufacturer_id' => 2
            ]
        );
        CarModel::create(
            [
                'name' => '3 series',
                'manufacturer_id' => 3
            ]
        );
        CarModel::create(
            [
                'name' => '5 series',
                'manufacturer_id' => 3
            ]
        );
        CarModel::create(
            [
                'name' => 'Cerato',
                'manufacturer_id' => 4
            ]
        );
        CarModel::create(
            [
                'name' => 'Sorento',
                'manufacturer_id' => 4
            ]
        );
    }
}
