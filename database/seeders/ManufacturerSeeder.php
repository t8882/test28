<?php

namespace Database\Seeders;

use App\Models\Manufacturer;
use Illuminate\Database\Seeder;

class ManufacturerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Manufacturer::create(
            [
                'name' => 'Toyota'
            ]
        );
        Manufacturer::create(
            [
                'name' => 'Honda'
            ]
        );
        Manufacturer::create(
            [
                'name' => 'BMW'
            ]
        );
        Manufacturer::create(
            [
                'name' => 'Kia'
            ]
        );
    }
}
