<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('manufacturer_id')->nullable(false);
            $table->unsignedBigInteger('model_id')->nullable(false);
            $table->unsignedInteger('year')->nullable();
            $table->unsignedInteger('mileage')->nullable();
            $table->string('color')->nullable();
            $table->timestamps();
            $table->foreign('manufacturer_id')
                ->references('id')
                ->on('manufacturers');
            $table->foreign('model_id')
                ->references('id')
                ->on('models');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cars');
    }
};
